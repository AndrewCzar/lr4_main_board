//#include "Arduino.h"

#include "Wire.h"
#include "defines.h"
#include "peri.h"
#include "motor.h"
#include "stdint.h"
#include "thresholds.h"
#include "whisker.h"
#include "EEPROM.h"
#include "esp_task_wdt.h"

#if ledDriver
  #include <SparkFunSX1509.h> // Include SX1509 library

// SX1509 I2C address (set by ADDR1 and ADDR0 (00 by default):
const byte SX1509_ADDRESS = 0x3E;  // SX1509 I2C address
SX1509 io; // Create an SX1509 object to be used throughout
// SX1509 Pin definition:
const byte SX1509_LED_PIN = 15;
const byte SX1509_LED_GREEN = 13;
#endif

volatile int errorHalt;


TaskHandle_t laserTask;
TaskHandle_t statusTask;


typedef struct{
  uint32_t timer;
  uint32_t state;
  int32_t catSeen; 
}catFault;

catFault laserFault;

//uint8_t bonnet = 0;
//uint8_t addr = 0;
//uint8_t LR4_state;


void setup() {
  
  Serial.begin(115200);

    #if I2C 
        delay(1500);
        Wire.begin(SDA,SCL);
        Wire.setClock(200000);
        
    #endif

    #if PERI
        init_peripherals();
        motor_init_pwm();
    #endif

#if ledDriver
    if (!io.begin(SX1509_ADDRESS))
  {
    while (1) ; // If we fail to communicate, loop forever.
  }
  //io.clock(INTERNAL_CLOCK_2MHZ, 4);
  
  io.pinMode(SX1509_LED_PIN, ANALOG_OUTPUT);
  io.pinMode(SX1509_LED_GREEN, ANALOG_OUTPUT);
  io.analogWrite(SX1509_LED_PIN,0);
  io.analogWrite(SX1509_LED_GREEN, 200);
  delay(2000);
  io.analogWrite(SX1509_LED_GREEN, 0);
#endif

  #if RTOS
      xTaskCreate(laserState,"lasers", 2048, NULL,2,&laserTask);
     //xTaskCreatePinnedToCore(laserState,"read vl53l1xs",4096,NULL, 1,&laserTask, 1); 
  #endif

  #if STATUS
    xTaskCreate(mainBoardStatus,"status", 2048, NULL,1,&statusTask);
  #endif

}

int local = 0;

//i2c rX buffers
uint16_t dta[3];
uint8_t dta8[3],rxData[3] = {0,0,0};
int stateError,ErrorTimer = 0;
//tX state machine case
uint8_t  globeLeds = 0;

uint8_t waitPIC = 0;

void loop() {
    
    if (rxData[0] == 0xCC){
      local = 0;
      errorHalt =  0;
       rxData[0] = 0x00;
       rxData[2] = 0x2C;
       while(!waitPIC) vTaskDelay(10 /portTICK_PERIOD_MS); //wait for pic to respond from rtos task
       //Serial.print("responded ");
       cycleLR4(&local);
       rxData[2] = 0xDD;
       local = 0;
       delay(200);
    }

    else if (read_BTN()){
    //  Serial.print("Button cycle \r");
          local = 0;
          errorHalt =  0;
          
          rxData[2] = 0x2C;
          rxData[0] = 0x00;
          
          while(!waitPIC) vTaskDelay(10 /portTICK_PERIOD_MS); //wait for pic to respond from rtos task
          cycleLR4(&local);
          rxData[2] = 0xDD;
          
          local = 0;
          delay(200);
    }
    
    else if (read_BTN2()){
      Serial.print("button empty \r");
        rxData[0] = 0x00;
        wait_empty();
        rxData[2] = 0xDD;
        delay(200);
    }
    
    else if(read_BTN3()){
      scanI2C();
    }
    else if(read_BTN4()){
      //  Serial.print("led \r");
        //io.analogWrite(SX1509_LED_GREEN, 255);
      ESP.restart();
    }
    delay(200);
}

uint8_t newRX = 0;

void laserState(void *param) {  
  printf("\nLR4-Prototype\n\n");
  //printf(__Time__);
  printf("Button 1: Cycle\n\n");
  printf("Button 2: Empty\n\n");
  printf("Button 3: Scan I2C\n\n");
  for(;;){ 
    switch(rxData[2])
      {
      case 0xDD:
        Serial.println("pic -> stopped");
        Wire.beginTransmission(0x10);  //tell pic not cycling 
        Wire.write(0xAB);
        Wire.endTransmission();
        rxData[2] = 0x00;
        waitPIC = 0;
        break;
      case 0xEE:
        Serial.println("pic -> stopped in motion");
        Wire.beginTransmission(0x10);  //tell pic not cycling 
        Wire.write(0xAA);
        Wire.endTransmission();
        rxData[2] = 0x00;
        waitPIC = 2;
        break;
      case 0x2C:
        Serial.println("pic -> moving");
        Wire.beginTransmission(0x10); // tell pic were cycling 
        Wire.write(0xBC);
        Wire.endTransmission();
        rxData[2] ++; 
        waitPIC = 1;
        break;

       default:
          //Serial.println("REquesting new data-> " );
          newRX = Wire.requestFrom(0x10,8);
          if (newRX){
            dta[0] = (uint8_t)Wire.read() << 8;
            dta[0] |= (uint8_t)Wire.read();
            dta[1] = (uint16_t)Wire.read() << 8;
            dta[1] |= (uint16_t)Wire.read();
            dta[2] = (uint16_t)Wire.read() << 8;
            dta[2] |= (uint16_t)Wire.read();
            
            //status
            dta8[0] = (uint8_t)Wire.read();
            dta8[1] =  (uint8_t)Wire.read();
            
            switch(dta8[0]){
              case 0xCC:
                if( mcpwm_get_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A) <= 0.0){
                  rxData[0] = 0xCC;
                  //waitPIC = 0;
                }
                break;
              case 0xEE:
                rxData[0] = 0xEE;
                rxData[2] = 0xEE;
                //stateError = local;
                //local = arc_done;
                errorHalt =  1;
                kill();
                break;
              
              case 0xCA:
                rxData[0] = 0xCA;
                break;
                
             default:
               break;
            }
            }
            break;
      }
      
    vTaskDelay(50 / portTICK_PERIOD_MS); 
   } 
  }

void mainBoardStatus(void* param){
  int IR,Vs,hll;
  for (;;){
    if (newRX){
      Serial.print(dta[0], DEC);
      Serial.print(", ");
      Serial.print(dta[1], DEC);
      Serial.print(", ");
      Serial.print(dta[2], DEC);
  
      Serial.print(", RX status: ");
      Serial.print(rxData[0], HEX);
      Serial.print(", ");
      Serial.print(dta8[0], HEX); 
      Serial.print(",PIC state is: ");
      Serial.print(dta8[1], HEX); 
      Serial.print(" PIC state told: ");
      Serial.print(waitPIC, HEX); 
      Serial.print(", ");
      Serial.println(rxData[2], HEX); 
      /*printf("%d, ",dta[0]);
      printf("%d, ",dta[1]);
      printf("%d, ",dta[2]);
      printf(" RX status:  %d, " , rxData[0]);
      printf("%d \r", dta8[0]);*/
      newRX = 0;
    }
    else Serial.println("Request for Data failed ");  
    

    /*hll = digitalRead(4);
    IR = analogRead(0);//adc2_get_raw(ADC2_CHANNEL_0,ADC_WIDTH_12Bit,&IR);
    adc2_get_raw(ADC2_CHANNEL_7,ADC_WIDTH_12Bit,&Vs);
    printf("\n\nHall    : %d\n",hll);
    printf("Bonnet  : %d\n",IR);
    printf("Vsupply : %d\n\n",Vs);*/
    vTaskDelay(200/portTICK_PERIOD_MS);
  }
}





void scanI2C(){
  byte error, address; //variable for error and I2C address
  int nDevices;

  Serial.println("Scanning...");

  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
      nDevices++;
    }
    else if (error == 4)
    {
      Serial.print("Unknown error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

  delay(1000); // wait 1 seconds for the next I2C scan
}
