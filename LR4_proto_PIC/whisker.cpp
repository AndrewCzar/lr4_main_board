#include "whisker.h"
#include "motor.h"
#include "peri.h"
#include "Wire.h"

//#include "soc/soc.h"
//#include "soc/rtc_cntl_reg.h"

void cycleLR4(int *local){
  // arc_home
  if (*local == 0) *local = new_cycle(arc_home);
  else  *local = new_cycle(*local);
  while (*local != arc_done && !errorHalt)
    {       
      *local = new_cycle(*local);
      vTaskDelay(10 / portTICK_PERIOD_MS);
    }
    
   if (errorHalt)  *local = arc_done;
    

}

int new_cycle(int position){
 //(!READ_PERI_REG(GPIO_OUT_REG) >> 24) | 0x01;
  static long delta_M1 = 0;
  static long delta_M2 = 0; //unsigned
  static long delta_HPD = 0;

if (errorHalt == 0)
  {
  switch (position)
  {
      //cw = 0
      //ccw = 1
    case (arc_home): 
        //if (!_ccw_)
       // {
          _cw_ = 0;
          _ccw_ = 1;
         #if c_DEBUG
          Serial.print("ccw set \r");
         #endif
          motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, 0.0);
          digitalWrite(phase,_ccw_); 
          motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0);
          if (!digitalRead(hall)) vTaskDelay(3000 / portTICK_PERIOD_MS);
          
          return home_predump;
        //}
        //else 
        
    //wait for hall, depending on direction enter different state 
    case (home_predump):
      #if c_DEBUG
        printf("home_predump  %d \r", errorHalt );
      #endif
        if (!digitalRead(hall) && _ccw_) 
        {
            delta_M1 = _millis();
            #if c_DEBUG
              Serial.print("first magnet found \r");
            #endif
            while(!digitalRead(hall));
            #if c_DEBUG
              Serial.print("Passed first magnet \r");
            #endif
            return arc_predump;
        }

        else if ( _cw_)// && (_millis() - delta_HPD) > 30000)
        {
          if (!digitalRead(hall)){
          #if c_DEBUG
            Serial.print("CW home \r");
          #endif
          return arc_level;
          }
          else return home_predump;
        }
        else 
        {
          
          return home_predump;
        }
        
    //found first magnet
    //determine next state

    case (arc_predump):
       #if c_DEBUG
        Serial.print("arc_predump \r");
       #endif
        if (_ccw_ && !digitalRead(hall)) 
        {
            delta_M2 = _millis();
            if ((delta_M2 - delta_M1) <= 4000 && (delta_M2 - delta_M1) >= 1500)
            {
              #if c_DEBUG
                Serial.print("Found 2nd magnet \r");
              #endif
             //hall_st = 0;
              return arc_dump;
            }
        }
        
        else if (_cw_ && !digitalRead(hall))  //cw
        {
        #if c_DEBUG
          Serial.print("CW arc predump \r");
        #endif
          hall_st = 0;
          delta_HPD = _millis();
          return home_predump;
        } 
        
        else if( (_millis() - delta_M1) >= 5000 && digitalRead(hall) && _ccw_) return home_predump;
        
        return arc_predump;
         
    //between first and second magnet
    case(predump_dump):
    #if c_DEBUG
      Serial.print("predump dump \r");
    #endif  
      if (_cw_)
        {
          delay(4000); //3000
          return home_predump;
        }

        if (_ccw_)  //cw
        {
          return arc_dump;
        }


    //clear magnet flag 
      //change to stop
      //wait 100 ms 
      //motor cw
      //set predump_dump
    case (arc_dump):
      #if c_DEBUG
        Serial.print("arc dump \r");
      #endif
      delay(3000); //////// new
      motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
      delay(1000);
      _cw_= 1;
      _ccw_ = 0;
      hall_st = 0;
      //kill();
      digitalWrite(phase,!_cw_);
      motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0);
      delay(3000);   //////// new   3000
      return predump_dump;

    case (arc_level):
      #if c_DEBUG
        Serial.print("leveling \r");
      #endif
      static int check = 0;
      delay(LEVEL_TIME_MAX);
      motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
      digitalWrite(phase,1);
      delay(1000);
        
      motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0); 

      while (digitalRead(hall)){
        delay(10);
        #if c_DEBUG
        Serial.print("..... \r");
        #endif
      }
      motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
      //kill();
      check = 0;
      return arc_done;
      // static int check = 0;
      // if (!check) {
      //   check = 1;
      //   delay(8500);
      //   motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
      //   digitalWrite(phase,1);
      //   delay(1000);
        
      //   motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0); 
      // }
      // if (!digitalRead(hall) && check){
      //   kill();
      //   check = 0;
      //   return arc_done;
      //}
      //else  return arc_level;
    
    case (arc_done):
    #if c_DEBUG
      Serial.print("Arc done \r");
      Serial.println("Arc done");
      Serial.println("Arc done");
      Serial.println("Arc done");
      Serial.println("Arc done");
      Serial.println("Arc done");
      Serial.println("Arc done");
      
    #endif
      kill();
      return arc_done;

    default:
    #if c_DEBUG
      Serial.print("Cycle - error \r");
    #endif  
    kill();
      

  }
  }
  else if (errorHalt == 1) {
    Serial.println("HALT");
    Serial.println("HALT");
    Serial.println("HALT");
    Serial.println("HALT");
    Serial.println("HALT");
    Serial.println("HALT");
    Serial.println("HALT");
  }
}




void empty(){ 
  static int exc = 1;
  digitalWrite(phase, clkw);
  motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0);                     
  if (exc == 1) delay(4000);                
  if (!read_Hall()) {                              
    delay(2000);                            
    exc = 1;
    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
    Serial.print("Empty Dump \r");
    //delay(1500);
    return;
  }
  else {
    exc++;
    empty();
  }
}

void empty_home(){
  static int exc = 1;
  digitalWrite(phase,cclkw);
  motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0);
  if (exc == 1) delay(8000);                   
  if (!read_Hall()) {                               
    exc = 1;                                     
    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);                   
    Serial.print("Empty Home \r");
    return;                                   
  }
  else {
    exc++;
    empty_home();
  }
}

void wait_empty(){ 
  static int c = 0;
  if (c == 0) empty();
  c++;                        
  if(read_BTN2() && c != 0) { 
    empty_home(); 
    c = 0;
    return;
    }      
  else wait_empty();
}
