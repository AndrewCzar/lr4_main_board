#include "Arduino.h"
#include "defines.h"
#include "thresholds.h"

#define c_DEBUG 0

volatile static uint8_t hall_st;
volatile static int _ccw_ = 0;  //(READ_PERI_REG(GPIO_OUT_REG) >> 24) & 0x01; 
volatile static int _cw_  = 0;  

extern volatile int errorHalt;

enum arc_position{
  arc_unknown,
  arc_home,
  home_predump,
  arc_predump,
  predump_dump,
  arc_dump,
  home_dump,
  arc_done,
  arc_level
};

enum empty_position{
  unknown_pos,
  home_pos,
  home_empty,
  empty_pos,
};

int new_cycle(int position);
void cycleLR4(int *local);

void wait_empty();
void empty_home();
void empty();

extern TaskHandle_t laserTask;
