#include "peri.h"

uint16_t read_IR(){
  //return analogRead(analogIR);
  int raw;
  adc2_get_raw(ADC2_CHANNEL_0,ADC_WIDTH_12Bit,&raw);
  return raw;
}

uint16_t read_Vin(){
  //return analogRead(Vin);
  int raw;
  adc2_get_raw(ADC2_CHANNEL_7,ADC_WIDTH_12Bit,&raw);
  return raw;

}

bool read_Hall(){
  return digitalRead(hall);//return gpio_get_level((gpio_num_t)hall);
}


bool read_BTN(){
  return !digitalRead(21); //gpio_get_level(GPIO_NUM_21);
}

bool read_BTN2(){
  return !digitalRead(17); //return !gpio_get_level(GPIO_NUM_17);
}

bool read_BTN3(){
  return !digitalRead(5);//return !gpio_get_level(GPIO_NUM_5);
}

bool read_BTN4(){
  return !digitalRead(15);
  //return !gpio_get_level(GPIO_NUM_15);
}

bool read_BTN5(){
  return !digitalRead(13); //return !gpio_get_level(GPIO_NUM_13);
}

void init_peripherals(){
    analogReadResolution(12);
    //attachInterrupt(digitalPinToInterrupt(hall), ISR, FALLING);
    init_hall();
    init_IR();
    init_Vin();
    init_buttons();
}

volatile uint32_t _millis(){
  return ((uint32_t)esp_timer_get_time()/1000) -33;
}


volatile uint32_t _micros(){
  return (uint32_t)(esp_timer_get_time() - 33);
}