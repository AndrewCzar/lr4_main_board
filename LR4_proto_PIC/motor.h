#include "Arduino.h"
#include "driver/mcpwm.h"
#include "peri.h"

void motor_init_pwm();
void motor_set_duty(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num , float duty_cycle);
void brushed_motor_stop(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num);
void rotate(int dir);
void kill();

void tone_m(int freq, int dur);


//motor control A/D
#define VPROP       36  //A
#define nFault      34  //A
#define phase       25  //D
#define nSleep      33  //D
#define enable1      32  //pwm

#define VPROP2      35
#define nFault2     39
#define phase2      12
#define nSleep2     14
#define enable2     26

#define cclkw        1  
#define clkw         0

// #define init_motor1(){\
//     pinMode(VPROP,INPUT);\
//     pinMode(nFault,INPUT);\
//     pinMode(nSleep,OUTPUT);\
//     pinMode(phase,OUTPUT);\
//     digitalWrite(nSleep,1);\
// }


#define init_motor1(){\
    gpio_set_direction(GPIO_NUM_36,GPIO_MODE_INPUT);\
    gpio_set_direction(GPIO_NUM_34,GPIO_MODE_INPUT);\
    gpio_set_direction(GPIO_NUM_33,GPIO_MODE_OUTPUT);\
    gpio_set_direction(GPIO_NUM_25,GPIO_MODE_OUTPUT);\
    gpio_set_level(GPIO_NUM_33,1);\
}

#define init_motor2(){ \
    pinMode(VPROP2,INPUT);\
    pinMode(nFault2,INPUT);\
    pinMode(nSleep2,OUTPUT);\
    pinMode(phase2,OUTPUT);\
    digitalWrite(nSleep2,1);\
    digitalWrite(phase2,0);\
}
