#ifndef _LR4_PERI
#define _LR4_PERI
#include "Arduino.h"
#include "driver/adc.h"
//#include "whisker.h"

#define RTC_addr 0x51
//#define Mux_addr 0x20

//#define laser

#define SDA 22
#define SCL 23

//#define Mux_interrupt 18

//analog
#define analogIR   0
#define Vin         27

//digital 
#define hall        4

#define BTN         21
#define BTN2        17
#define BTN3        5
#define BTN4        15
#define BTN5        13

//init

#define init_hall() pinMode(hall,INPUT_PULLUP) //_PULLUP)
#define init_IR()   pinMode(analogIR,INPUT)
#define init_Vin()  pinMode(Vin,INPUT)

#define init_buttons()  {\
    pinMode(BTN,INPUT_PULLUP);\
    pinMode(BTN2,INPUT_PULLUP);\
    pinMode(BTN3,INPUT_PULLUP);\
    pinMode(BTN4,INPUT_PULLUP);\
}

//pinMode(BTN5,INPUT_PULLUP);\

uint16_t read_IR();
uint16_t read_Vin();
bool read_Hall();
bool read_BTN();
bool read_BTN2();
bool read_BTN3();
bool read_BTN4();
bool read_BTN5();
void init_peripherals();
volatile uint32_t _millis();
volatile uint32_t _micros();

#endif 