#ifndef thres_h
#define thres_h

#define cat_thres           200 //200 
#define cat_delta           50 // 150
#define cat_delay           420000 //10000 
#define cat_restart_delay   15000

#define cat_detected   15

#define LEVEL_TIME_MAX      8000
#define DUMP_TIME_MAX       3000

#define LEVEL_TIME_MIN      7000
#define DUMP_TIME_MIN       2000

#define past_dump           1000 //1500

#define DFI_START           4.75*past_dump
#define DFI_STOP            7.75*past_dump  

//extern uint8_t DFI_flag = 0;

// uint16_t mode = 1;
// uint16_t timing  = 50; 
// uint16_t offset  = 0; 
// uint16_t x = 16;
// uint16_t y = 16;

#endif




