#include "motor.h"

void motor_init_pwm(){
    init_motor1();
    //init_motor2();

    mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, enable1);
    mcpwm_config_t pwm_config;
    pwm_config.frequency = 10000;    //frequency = 500Hz,
    pwm_config.cmpr_a = 0;          //duty cycle of PWMxA = 0
    pwm_config.cmpr_b = 0;          //duty cycle of PWMxb = 0
    pwm_config.counter_mode = MCPWM_UP_COUNTER;
    pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
    mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);
}

//void motor_set_duty(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num , float duty_cycle, int direction)

void motor_set_duty(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num , float duty_cycle)
{
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_B);
    mcpwm_set_duty(mcpwm_num, timer_num, MCPWM_OPR_A, duty_cycle);
    mcpwm_set_duty_type(mcpwm_num, timer_num, MCPWM_OPR_A, MCPWM_DUTY_MODE_0); //call this each time, if operator was previously in low/high state
}

void brushed_motor_stop(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num)
{
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_A);
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_B);
}



void kill() {
    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
}

void rotate(int dir){
//state mahcine for direciton 
        switch (dir){
            case 0:
                if (read_BTN3())
                {
                    Serial.println("cw");
                    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
                    digitalWrite(phase,1);
                    delay(5);
                    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0); //empty();        
                    dir = 1;
                    delay(100);
                }
            case 1:
                if (read_BTN3())
                {
                    Serial.println("ccw");
                    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
                    digitalWrite(phase,0);
                    delay(5);
                    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,100.0); //empty();        
                    dir = 0;
                    delay(100);
                }
        }
}

void tone_m(int freq, int dur){
    //int dur_2 = millis();
    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,20.0);
    mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_0, freq);
    delay(dur);
    //while ((millis() - dur_2) <= dur);
    mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_0, (uint32_t)1000);
    motor_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0,0.0);
}
